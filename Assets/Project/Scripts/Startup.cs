﻿using UnityEngine;
using UnityEditor;

//[InitializeOnLoad]
public class Startup
{

    static Startup()
    {
        Debug.Log("Up and running");
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 100;
    }
    
}
