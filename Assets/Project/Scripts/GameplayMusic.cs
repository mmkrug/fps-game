﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayMusic : MonoBehaviour
{
    SoundManager soundManager;
    GameManager gameManager;
    AudioSource audioSource;

    bool isHordeTime = false;
    int nowPlaying = 0;

    // Start is called before the first frame update
    void Start()
    {
        soundManager = SoundManager.Instance;
        gameManager = GameManager.Instance;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isHordeTime)
        {
            // horde time, 1st track playing
            if( !audioSource.isPlaying)
            {
                // track finished
                switch (nowPlaying)
                {
                    case 1:
                        audioSource.PlayOneShot(soundManager.hordeMusic2);
                        nowPlaying = 2;
                        break;

                    case 2:
                        audioSource.PlayOneShot(soundManager.hordeMusic3);
                        nowPlaying = 3;
                        break;

                    case 3:
                        audioSource.PlayOneShot(soundManager.hordeMusic4);
                        nowPlaying = 4;
                        break;

                    case 4:
                        audioSource.PlayOneShot(soundManager.hordeMusic3);
                        nowPlaying = 3;
                        break;

                    default:
                        break;

                }
            }


        } else
        {
            // detect moment when horde starts
            if (gameManager.wave >= gameManager.waveForceChase)
            {   
                // stop already playing music
                audioSource.Stop();

                // play initial horde track
                audioSource.PlayOneShot(soundManager.hordeMusic1);
                nowPlaying = 1;
                isHordeTime = true;
            }
        }





        
    }
}
