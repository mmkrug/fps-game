﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseVertical : MonoBehaviour
{

    public float value;

    Slider mouseVerticalSlider;

    void Start()
    {
        mouseVerticalSlider = GetComponent<Slider>();

        if( ! PlayerPrefs.HasKey("mouseVertical"))
        {
            PlayerPrefs.SetFloat("mouseVertical", 0.3f);
            Load();
        } else
        {
            //Debug.Log(PlayerPrefs.GetFloat("mouseVertical"));
            Load();
        }

    }



    public void ChangeMouseVertical()
    {
        Save();
    }


    void Load()
    {
        mouseVerticalSlider.value = PlayerPrefs.GetFloat("mouseVertical");
    }

    void Save()
    {
        PlayerPrefs.SetFloat("mouseVertical", mouseVerticalSlider.value);
    }

}
