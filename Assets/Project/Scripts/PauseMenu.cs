﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public GameObject pauseMenu;

    public static bool gameIsPaused;


    GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && ! Player.playerIsDead)
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameManager.ActivateGunScript();
        gameIsPaused = false;

        if (Cursor.lockState == CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }



        // updating mouse sensitivities
        if (!PlayerPrefs.HasKey("mouseVertical"))
        {
            PlayerPrefs.SetFloat("mouseVertical", 0.3f);
            gameManager.ChangeVerticalSensitivity(PlayerPrefs.GetFloat("mouseVertical") * 1000);
        }
        else
        {
            gameManager.ChangeVerticalSensitivity(PlayerPrefs.GetFloat("mouseVertical") * 1000);
        }

        if (!PlayerPrefs.HasKey("mouseHorizontal"))
        {
            PlayerPrefs.SetFloat("mouseHorizontal", 0.25f);
            gameManager.ChangeHorizontalSensitivity(PlayerPrefs.GetFloat("mouseVertical") * 1000);
        }
        else
        {
            gameManager.ChangeHorizontalSensitivity(PlayerPrefs.GetFloat("mouseVertical") * 1000);
        }
    }

    void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
        gameManager.DeactivateGunScript();

        if (Cursor.lockState == CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }



    //public void PlayGame()
    //{
    //    SceneManager.LoadScene(1);
    //}

    public void QuitGame()
    {
        Application.Quit();
    }

    public void RestartGame()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameManager.ActivateGunScript();
        gameIsPaused = false;

        if (Cursor.lockState == CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        Player.playerIsDead = false;
        //Resume();
    }

    public void ChangeSceneToMainMenu()
    {
        SceneManager.LoadScene(0);
        Player.playerIsDead = false;
        gameIsPaused = false;

    }

}
