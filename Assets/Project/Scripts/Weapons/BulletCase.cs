﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCase : MonoBehaviour
{

    public Vector2 forceRange;
    public Vector2 rotationRange;

    Rigidbody rigidbody;
    SoundManager soundManager;
    bool alreadyHit;
    public Vector3 direction;

    // Start is called before the first frame update
    void Awake()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
        
    }

    private void Start()
    {
        soundManager = SoundManager.Instance;
    }

    private void OnEnable()
    {
        alreadyHit = false;
        //shellCase.GetComponent<BulletCase>().SetMinMax(60, 720);



        //shellCase.GetComponent<BulletCase>().SetMinMax(60, 720);

        //shellCase.GetComponent<Rigidbody>().AddForce(
        //    shellCase.transform.forward +
        //    shellCase.transform.right +
        //    shellCase.transform.up
        //    );


        rigidbody.AddTorque(new Vector3(
            Random.Range(rotationRange.x, rotationRange.y), 
            Random.Range(rotationRange.x, rotationRange.y), 
            Random.Range(rotationRange.x, rotationRange.y)));
    }



    private void OnCollisionEnter(Collision collision)
    {

        //collision.get
        // if didnt touch anything yet, play the sound on impact
        if ( ! alreadyHit && collision.gameObject.tag != "Player")
        {
            if (Random.value < 0.5)
            {
                soundManager.PlayBulletCaseSound(1, transform.position);
            }
            else
            {
                soundManager.PlayBulletCaseSound(2, transform.position);
            }
            
            alreadyHit = true;

        }


    }

    public void SetDirection( Vector3 direction)
    {
        this.direction = direction;
    }
}
