﻿using UnityEngine;

public class WeaponSwitch : MonoBehaviour
{

    //public int selectedWeapon = 0;

    // Start is called before the first frame update
    void Start()
    {
        SelectWeapon(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SelectWeapon(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SelectWeapon(1);
        }


    }

    void SelectWeapon(int n)
    {
        if (transform.childCount < n)
        {
            return;
        }

        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == n)
            {
                weapon.gameObject.SetActive(true);
            } else
            {
                weapon.gameObject.SetActive(false);
            }

            i++;
        }
    }
}
