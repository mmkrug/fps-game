﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSway : MonoBehaviour
{

    public Transform camera;
    public float xintensity;
    public float yintensity;
    public float smooth;
    private Quaternion origin_rotation;
    // Start is called before the first frame update
    void Start()
    {
        origin_rotation = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        //gameObject.transform.Rotate(Vector3.up);
        //Vector3.Lerp(intensity, Vector3.zero, deceleration * Time.deltaTime);

        float t_x_mouse = Input.GetAxis("Mouse X");
        float t_y_mouse = Input.GetAxis("Mouse Y");

        Quaternion t_x_adj = Quaternion.AngleAxis(-xintensity * t_x_mouse, Vector3.up);
        Quaternion t_y_adj = Quaternion.AngleAxis(yintensity * t_y_mouse, Vector3.right);
        Quaternion target_rotation = origin_rotation * t_x_adj * t_y_adj;

        transform.localRotation = Quaternion.Lerp(transform.localRotation, target_rotation, Time.deltaTime * smooth);
        //transform.rotation = Quaternion.Lerp(transform.rotation, target_rotation, Time.deltaTime * smooth);
    }
}
