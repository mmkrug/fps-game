﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAdditions : MonoBehaviour
{

    public Gun gun;

    public Animator animator;
    public Camera playerMainCamera;
    public float cameraLerpSpeed = 2.0f;

    private float defaultCameraFOV = 60f;

    // Start is called before the first frame update
    void Start()
    {
        this.defaultCameraFOV = this.playerMainCamera.fieldOfView;
    }

    private void Update()
    {

        
    }

    private void OnEnable()
    {
        Debug.Log("enabled");
    }

    void FixedUpdate()
    {
        
        if (Input.GetMouseButton(1))
        {
            animator.SetBool("Zooming", true);
            this.playerMainCamera.fieldOfView = Mathf.Lerp(this.playerMainCamera.fieldOfView, defaultCameraFOV - 20, cameraLerpSpeed * Time.deltaTime);
            //this.playerMainCamera.fieldOfView = this.defaultCameraFOV - 20;
            //Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, targetFOV, speed * Time.deltaTime);
        } else
        {
            animator.SetBool("Zooming", false);
            this.playerMainCamera.fieldOfView = Mathf.Lerp(this.playerMainCamera.fieldOfView, defaultCameraFOV, cameraLerpSpeed * Time.deltaTime);
            //this.playerMainCamera.fieldOfView = this.defaultCameraFOV;
        }




    }
}
