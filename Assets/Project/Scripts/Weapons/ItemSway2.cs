﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSway2 : MonoBehaviour
{

    //public Transform camera;
    public float xintensity;
    public float yintensity;
    public float smooth;
    public float maxSway;
    private Vector3 initialPosition;
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        mouseX = Mathf.Clamp(mouseX, -maxSway, maxSway);
        mouseY = Mathf.Clamp(mouseY, -maxSway, maxSway);


        Vector3 finalPosition = new Vector3(-mouseX * xintensity, -mouseY * yintensity, 0);
        transform.localPosition = Vector3.Lerp(transform.localPosition, finalPosition + initialPosition, Time.deltaTime * smooth);

    }
}
