﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Gun : MonoBehaviour
{
    [Header("-Gun Owner-")]
    public Player player;

    [Header("-Gun general info-")]
    public string name;
    public string type;

    [Header("Gun Stats")]
    public int damage;
    public float equipTime;
    public float zoomLerpTime;
    public float timeBetweenShooting, spread, range, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold;
    public int bulletsLeft, bulletsShot;

    // bools and other variables
    SoundManager soundManager;
    ObjectPoolingManager objectPoolingManager;
    GameManager gameManager;
    Zombie tempZombie;
    bool shooting, readyToShoot, reloading;
    float defaultSpread;
    float defaultCameraFOV;


    [Header("References")]
    public Animator animator;
    public Camera fpsCamera;
    public Camera itemRenderCamera;
    public Transform weaponZoomPoint;
    public Transform bulletSpawnPoint;
    public RaycastHit rayHit;
    public LayerMask collisionLayers;
    public AudioSource audioSource;
    public Transform shellSpawnPoint;

    [Header("Graphic References")]
    public GameObject bulletHoleGraphic;
    public ParticleSystem muzzleFlashParticleSystem;
    //public TextMeshProUGUI ammoText;


    [Header("Camera Recoil")]
    public Transform cameraRecoilPoint;
    public float cameraRotationSpeed = 6;
    public float cameraReturnSpeed = 25;
    public Vector3 cameraRecoilRotation = new Vector3(2f, 2f, 2f);
    public Vector3 cameraRecoilRotationAiming = new Vector3(0.5f, 0.5f, 0.5f);
    public bool aiming;
    private Vector3 cameraCurrentRotation;
    private Vector3 cameraRot;

    [Header("Gun Recoil")]
    public Transform gunRecoilPosition;     // gun position
    public Transform gunRotationPoint;      // item pivot point
    public float gunPositionalRecoilSpeed = 8f;
    public float gunRotationalRecoilSpeed = 8f;
    public float gunPositionalReturnSpeed = 18f;
    public float gunRotationalReturnSpeed = 38f;
    public Vector3 gunRecoilRotation = new Vector3(10, 5, 7);
    public Vector3 gunRecoilKickBack = new Vector3(0.015f, 0f, -0.2f);
    public Vector3 gunRecoilRotationAim = new Vector3(10, 4, 6);
    public Vector3 gunRecoilKickBackAim = new Vector3(0.015f, 0f, -0.2f);
    Vector3 gunRotationalRecoil;
    Vector3 gunPositionalRecoil;
    Vector3 gunRot;

    private void Awake()
    {
        defaultCameraFOV = fpsCamera.fieldOfView;
        defaultSpread = spread;
        bulletsLeft = magazineSize;
        readyToShoot = true;
    }

    private void Start()
    {
        objectPoolingManager = ObjectPoolingManager.Instance;
        soundManager = SoundManager.Instance;
        gameManager = GameManager.Instance;
    }

    private void OnEnable()
    {
        if (PauseMenu.gameIsPaused)
        {
            return;
        }


        player.equippedWeaponType = type;


        // Play Equip Sound
        if (SoundManager.Instance)
        {
            // used variable to block ability to shoot
            reloading = true;
            animator.applyRootMotion = false;


            SoundManager.Instance.PlayEquipSound(type);
            Invoke("SetAnimReloadOff", equipTime - 0.3f);
            Invoke("EquippingFinished", equipTime);
        }
    }

    private void Update()
    {
        MyInput();
    }

    void FixedUpdate()
    {
        // constant camera movement to the default rotation point
        cameraCurrentRotation = Vector3.Lerp(cameraCurrentRotation, Vector3.zero, cameraReturnSpeed * Time.deltaTime);
        cameraRot = Vector3.Slerp(cameraRot, cameraCurrentRotation, cameraRotationSpeed * Time.fixedDeltaTime);
        cameraRecoilPoint.transform.localRotation = Quaternion.Euler(cameraRot);



        // constant gun movement to the default rotation and position point
        if (aiming)
        {
            gunRotationalRecoil = Vector3.Lerp(gunRotationalRecoil, weaponZoomPoint.localEulerAngles, gunRotationalReturnSpeed * 2 * Time.deltaTime);
            gunPositionalRecoil = Vector3.Lerp(gunPositionalRecoil, weaponZoomPoint.localPosition, gunPositionalReturnSpeed * 2 * Time.deltaTime);
            gunRecoilPosition.localPosition = Vector3.Slerp(gunRecoilPosition.localPosition, gunPositionalRecoil, gunPositionalRecoilSpeed * 2 * Time.fixedDeltaTime);
        }
        else
        {
            gunRotationalRecoil = Vector3.Lerp(gunRotationalRecoil, Vector3.zero, gunRotationalReturnSpeed * Time.deltaTime);
            gunPositionalRecoil = Vector3.Lerp(gunPositionalRecoil, Vector3.zero, gunPositionalReturnSpeed * Time.deltaTime);
            gunRecoilPosition.localPosition = Vector3.Slerp(gunRecoilPosition.localPosition, gunPositionalRecoil, gunPositionalRecoilSpeed * Time.fixedDeltaTime);
        }



        gunRot = Vector3.Slerp(gunRot, gunRotationalRecoil, gunRotationalRecoilSpeed * Time.fixedDeltaTime);
        gunRotationPoint.localRotation = Quaternion.Euler(gunRot);
    }



    private void MyInput()
    {
        // Mouse 0, setting "shooting" bool as .GetKey or .GetKeyDown
        if (allowButtonHold)
        {
            shooting = Input.GetKey(KeyCode.Mouse0);

        }
        else
        {
            shooting = Input.GetKeyDown(KeyCode.Mouse0);
        }


        // Shoot, when player is able to shoot
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            animator.applyRootMotion = true;
            Shoot();
        }


        // Reload, when player is able to reload
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading)
        {
            animator.applyRootMotion = false;
            Reload();
        }


        if (player.isMoving)
        {
            if (player.isSprinting)
            {
                spread = defaultSpread * 2;
            }
            else
            {
                spread = defaultSpread * 1.4f;
            }
        }
        else
        {
            spread = defaultSpread;
        }


        // Right mouse button, zooming
        if (Input.GetMouseButton(1) && !reloading)
        {
            if(player.isSprinting) return;
            animator.SetBool("Zooming", true);

            aiming = true;
            player.isZooming = true;
            animator.applyRootMotion = true;
            spread = defaultSpread * 0.1f;
            this.fpsCamera.fieldOfView = Mathf.Lerp(this.fpsCamera.fieldOfView, defaultCameraFOV - 20, zoomLerpTime * Time.deltaTime);
            this.itemRenderCamera.fieldOfView = Mathf.Lerp(this.fpsCamera.fieldOfView, defaultCameraFOV - 20, zoomLerpTime * Time.deltaTime);
            //transform.localPosition = Vector3.Lerp(transform.localPosition, weaponZoomPoint.localPosition, 2 * Time.deltaTime);

        }
        else
        {
            if (player.isSprinting) return;
            animator.SetBool("Zooming", false);

            aiming = false;
            player.isZooming = false;
            //spread = defaultSpread;
            this.fpsCamera.fieldOfView = Mathf.Lerp(this.fpsCamera.fieldOfView, defaultCameraFOV, zoomLerpTime * Time.deltaTime);
            this.itemRenderCamera.fieldOfView = Mathf.Lerp(this.itemRenderCamera.fieldOfView, defaultCameraFOV, zoomLerpTime * Time.deltaTime);

        }





        // sprinting ??


    }


    private void Shoot()
    {
        readyToShoot = false;
        GameObject tempObject;

        // Randomize bullet spread
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        // Calculate Direction with Spread
        //Vector3 direction = fpsCamera.transform.forward + new Vector3(x, y, 0);
        Vector3 direction = fpsCamera.transform.forward + fpsCamera.transform.right * x + fpsCamera.transform.up * y;


        // Shoot RayCast
        RaycastHit[] hits = Physics.RaycastAll(fpsCamera.transform.position, direction, collisionLayers);


        Debug.DrawRay(fpsCamera.transform.position, direction * 200, Color.red, 20);
        // sorting hits in order of distance
        Helper.Instance.sortHits(hits);

        foreach (RaycastHit hit in hits)
        {
            //Debug.Log(hit.transform.tag);
        }

        foreach (RaycastHit hit in hits)
        {


            tempObject = hit.transform.gameObject;


            if (hit.transform.tag.Equals("Zombie_Head"))
            {
                //Debug.Log("in zombie head");
                while (tempObject.transform.GetComponent<Zombie>() == null)
                {
                    tempObject = tempObject.transform.parent.gameObject;
                }
                //tempZombie = tempObject.transform.GetComponent<Zombie>();
                //tempZombie.TakeDamage(damage * 2);
                //gameManager.triggerHitMarker(tempZombie.isDead, "head");

                //hit.rigidbody.AddForceAtPosition(direction * 5000, hit.point);



                //tempObject = objectPoolingManager.spawnFromPool("FleshHolesPool");
                //tempObject.transform.position = hit.point;
                //tempObject.transform.rotation = Quaternion.LookRotation(hit.normal);
                //tempObject.transform.SetParent(hit.rigidbody.transform);

                tempZombie = tempObject.transform.GetComponent<Zombie>();
                //tempZombie.TakeDamage(damage);
                //gameManager.triggerHitMarker(tempZombie.isDead, "body");


                // spawn bullet hole
                tempObject = objectPoolingManager.spawnFromPool("FleshHolesPool");
                tempObject.transform.position = hit.point;
                tempObject.transform.rotation = Quaternion.LookRotation(hit.normal);
                tempObject.transform.SetParent(hit.rigidbody.transform);



                // damage management
                if ( ! tempZombie.isDead)
                {
                    tempZombie.TakeDamage(damage * 2);
                    gameManager.triggerHitMarker(tempZombie.isDead, "head");

                    if (tempZombie.isDead) gameManager.scoreKilled("head");
                }

                // apply hit force
                hit.rigidbody.AddForceAtPosition(direction * 5000, hit.point);


                break;
            }



            if (hit.transform.tag.Equals("Zombie_Body"))
            {
                //Debug.Log("in zombie body");
                while (tempObject.transform.GetComponent<Zombie>() == null)
                {
                    tempObject = tempObject.transform.parent.gameObject;
                }
                tempZombie = tempObject.transform.GetComponent<Zombie>();
                //tempZombie.TakeDamage(damage);
                //gameManager.triggerHitMarker(tempZombie.isDead, "body");
                

                // spawn bullet hole
                tempObject = objectPoolingManager.spawnFromPool("FleshHolesPool");
                tempObject.transform.position = hit.point;
                tempObject.transform.rotation = Quaternion.LookRotation(hit.normal);
                tempObject.transform.SetParent(hit.rigidbody.transform);

                // damage management
                if (!tempZombie.isDead)
                {
                    tempZombie.TakeDamage(damage);
                    gameManager.triggerHitMarker(tempZombie.isDead, "body");

                    if (tempZombie.isDead) gameManager.scoreKilled("body");
                }

                // apply hit force
                hit.rigidbody.AddForceAtPosition(direction * 5000, hit.point);

                break;
            }





            if (hit.transform.tag.Equals("Metal"))
            {
                tempObject = objectPoolingManager.spawnFromPool("MetalHolesPool");
                tempObject.transform.position = hit.point;
                tempObject.transform.rotation = Quaternion.LookRotation(hit.normal);
                soundManager.PlayRicocheteSound("hard", hit.point);
                break;
            }

            if (hit.transform.tag.Equals("Stone"))
            {
                tempObject = objectPoolingManager.spawnFromPool("StoneHolesPool");
                tempObject.transform.position = hit.point;
                tempObject.transform.rotation = Quaternion.LookRotation(hit.normal);
                soundManager.PlayRicocheteSound("soft", hit.point);
                break;
            }

            if (hit.transform.tag.Equals("Wood"))
            {
                tempObject = objectPoolingManager.spawnFromPool("WoodHolesPool");
                tempObject.transform.position = hit.point;
                tempObject.transform.rotation = Quaternion.LookRotation(hit.normal) * Quaternion.Euler(0, 0, Random.Range(0, 360));
                soundManager.PlayRicocheteSound("soft", hit.point);
                break;
            }
        }

        // Play Sound
        soundManager.PlayShotSound(type);

        //spawn shell
        var shellCase = objectPoolingManager.spawnFromPool("BulletShellPool");
        shellCase.transform.position = shellSpawnPoint.position;
        shellCase.transform.rotation = shellSpawnPoint.rotation * Quaternion.Euler(0,-90,0);

        shellCase.GetComponent<Rigidbody>().AddForce(
            shellCase.transform.forward * Random.Range(1f,2f) +
            shellCase.transform.right * Random.Range(1f, 2f) +
            shellCase.transform.up * Random.Range(1f, 2f)
            );


        if (type == "rifle")
        {
            shellCase.transform.localScale = new Vector3(1,1,2);
        }

        // Camera Recoil
        ApplyCameraRecoil();

        // Gun Recoil
        ApplyGunRecoil();

        muzzleFlashParticleSystem.Clear();
        muzzleFlashParticleSystem.Play();

        bulletsLeft--;
        Invoke("ResetShot", timeBetweenShooting);

    }

    private void ResetShot()
    {
        readyToShoot = true;
    }

    private void Reload()
    {
        switch (type)
        {
            case "pistol":
                if (player.pistolAmmo == 0)
                    return;
                break;
            case "rifle":
                if (player.rifleAmmo == 0)
                    return;
                break;
        }


        animator.SetBool("Reloading", true);
        reloading = true;

        // Play Sound
        SoundManager.Instance.PlayReloadSound(type);

        Invoke("SetAnimReloadOff", reloadTime - 0.3f);
        Invoke("ReloadFinished", reloadTime);
    }

    private void ReloadFinished()
    {
        switch (type)
        {
            case "pistol":
                if (player.pistolAmmo + bulletsLeft > magazineSize)
                {
                    // enough ammo to fill magazine
                    player.pistolAmmo -= magazineSize - bulletsLeft;
                    bulletsLeft = magazineSize;
                } else
                {
                    // not enough ammo to fill magazine
                    bulletsLeft = bulletsLeft + player.pistolAmmo;
                    player.pistolAmmo = 0;
                }
                break;

            case "rifle":
                if (player.rifleAmmo + bulletsLeft > magazineSize)
                {
                    // enough ammo to fill magazine
                    player.rifleAmmo -= magazineSize - bulletsLeft;
                    bulletsLeft = magazineSize;
                }
                else
                {
                    // not enough ammo to fill magazine
                    bulletsLeft = bulletsLeft + player.rifleAmmo;
                    player.rifleAmmo = 0;
                }
                break;

            default:
                break;
        }


        reloading = false;
    }

    private void SetAnimReloadOff()
    {
        animator.SetBool("Reloading", false);
    }

    private void EquippingFinished()
    {
        //animator.Play("Idle");
        //animator.enabled = false;
        reloading = false;
    }

    private void ApplyCameraRecoil()
    {
        if (aiming)
        {
            // apply random rotation to the camera when aiming (right click)
            cameraCurrentRotation += new Vector3(-cameraRecoilRotationAiming.x,
                                                     Random.Range(-cameraRecoilRotationAiming.y, cameraRecoilRotationAiming.y),
                                                     Random.Range(-cameraRecoilRotationAiming.z, cameraRecoilRotationAiming.z));
        }
        else
        {
            // apply random rotation to the camera
            cameraCurrentRotation += new Vector3(-cameraRecoilRotation.x,
                                                    Random.Range(-cameraRecoilRotation.y, cameraRecoilRotation.y),
                                                    Random.Range(-cameraRecoilRotation.z, cameraRecoilRotation.z));
        }
    }

    private void ApplyGunRecoil()
    {
        if (aiming)
        {
            // apply random rotation to the weapon when aiming (right click)
            gunRotationalRecoil += new Vector3(-gunRecoilRotationAim.x,
                                                    Random.Range(-gunRecoilRotationAim.y,gunRecoilRotationAim.y),
                                                    Random.Range(-gunRecoilRotationAim.z, gunRecoilRotationAim.z));

            // apply random offset to the weapon when aiming (right click)
            gunPositionalRecoil += new Vector3(Random.Range(-gunRecoilKickBackAim.x, gunRecoilKickBackAim.x),
                                                    Random.Range(0, gunRecoilKickBackAim.y),
                                                    gunRecoilKickBackAim.z);
        }
        else
        {
            // apply random rotation to the weapon
            gunRotationalRecoil += new Vector3(-gunRecoilRotation.x,
                                                    Random.Range(-gunRecoilRotation.y, gunRecoilRotation.y),
                                                    Random.Range(-gunRecoilRotation.z, gunRecoilRotation.z));

            // apply random offset to the weapon
            gunPositionalRecoil += new Vector3(Random.Range(-gunRecoilKickBack.x, gunRecoilKickBack.x),
                                                    Random.Range(-gunRecoilKickBack.y, gunRecoilKickBack.y),
                                                    gunRecoilKickBack.z);
        }
    }

}
