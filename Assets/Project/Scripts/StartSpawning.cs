﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSpawning : MonoBehaviour
{

    public GameObject player;

    GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;

        transform.position = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        //hide pick up panel

        Player tempItem = other.gameObject.GetComponent<Player>();

        if (tempItem)
        {
            // player has left the area
            gameManager.SpawnZombies(30);
            gameManager.wave = 1;
            this.gameObject.SetActive(false);
        }
    }

}
