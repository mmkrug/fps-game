﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecalDeactivator : MonoBehaviour
{

    public float lifeTime = 5.0f;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(lifeTime);
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        StartCoroutine(Start());
    }

}