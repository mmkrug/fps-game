﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseHorizontal : MonoBehaviour
{

    Slider mouseHorizontalSlider;

    void Start()
    {
        mouseHorizontalSlider = GetComponent<Slider>();

        if( ! PlayerPrefs.HasKey("mouseHorizontal"))
        {
            PlayerPrefs.SetFloat("mouseHorizontal", 0.25f);
            Load();
        } else
        {
            //Debug.Log(PlayerPrefs.GetFloat("mouseHorizontal"));
            Load();
        }

    }



    public void ChangeMouseHorizontal()
    {
        Save();
    }


    void Load()
    {
        mouseHorizontalSlider.value = PlayerPrefs.GetFloat("mouseHorizontal");
    }

    void Save()
    {
        PlayerPrefs.SetFloat("mouseHorizontal", mouseHorizontalSlider.value);
    }

}
