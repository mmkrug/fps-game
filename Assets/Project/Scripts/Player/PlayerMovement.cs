﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Player player;
    public Camera playerCamera;
    public Camera itemRenderCamera;
    public CharacterController controller;
    public Transform swingPosition;

    public float defaultSpeed = 8f;
    public float sprintSpeed = 12f;
    public float courchSpeed = 4f;
    public float gravity = -9.81f * 2.5f;
    public float jumpHeight = 3f;
    public float cameraLerpSpeed = 10f;
    public float cameraSprintFOVDifference = 20f;
    public float crouchHeightModifier = 0.5f;
    public float footstepFrequencyModifier = 1.5f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;



    CapsuleCollider playerCollider;
    Transform rightSwingPosition;
    Transform leftSwingPosition;
    Transform defaultSwingPosition;
    SoundManager soundManager;
    AudioSource audioSource;

    Vector3 velocity;
    float currentSpeed;
    public bool isGrounded;
    bool isJumping;
    bool isSprinting;
    bool isZooming;
    float defaultCameraFOV;
    float footStepCooldown;
    bool footstepReady = true;

    

    private void Start()
    {
        defaultCameraFOV = playerCamera.fieldOfView;
        currentSpeed = defaultSpeed;
        rightSwingPosition = swingPosition.Find("Right Swing Position");
        leftSwingPosition = swingPosition.Find("Left Swing Position");
        defaultSwingPosition = swingPosition.Find("Default Swing Position");
        soundManager = SoundManager.Instance;
        audioSource = GetComponent<AudioSource>();


    }

    // Update is called once per frame
    void Update()
    {
        var q = Input.GetKey(KeyCode.Q);
        var e = Input.GetKey(KeyCode.E);

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
            isJumping = false;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if (x > 0.001f
            || x < -0.01f
            || z > 0.01f
            || z < -0.01f)
        {
            player.isMoving = true;

            if (isGrounded)
                this.PlayFootstepSound();

        } else
        {
            player.isMoving = false;
        }

        // apply movement
        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * currentSpeed * Time.deltaTime);

        //jumping 
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            isJumping = true;
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }

        // sprinting
        if (Input.GetKey(KeyCode.LeftShift)
            && (z > 0)
            && ( ! isJumping)
            && ( ! player.isZooming))
        {
            player.isSprinting = true;
            currentSpeed = sprintSpeed;
            playerCamera.fieldOfView = Mathf.Lerp(playerCamera.fieldOfView, defaultCameraFOV + cameraSprintFOVDifference, cameraLerpSpeed * Time.deltaTime);
        }
        else
        {
            currentSpeed = defaultSpeed;
            player.isSprinting = false;
        }




        if (q){
            swingPosition.transform.localPosition = Vector3.Lerp(swingPosition.transform.localPosition, leftSwingPosition.localPosition, 10 * Time.deltaTime);
            swingPosition.transform.localRotation = Quaternion.Lerp(swingPosition.transform.localRotation, leftSwingPosition.localRotation, 10 * Time.deltaTime);
        } 
        else if (e)
        {
            swingPosition.transform.localPosition = Vector3.Lerp(swingPosition.transform.localPosition, rightSwingPosition.localPosition, 10 * Time.deltaTime);
            swingPosition.transform.localRotation = Quaternion.Lerp(swingPosition.transform.localRotation, rightSwingPosition.localRotation, 10 * Time.deltaTime);
        }
        else
        {
            swingPosition.transform.localPosition = Vector3.Lerp(swingPosition.transform.localPosition, defaultSwingPosition.localPosition, 10 * Time.deltaTime);
            swingPosition.transform.localRotation = Quaternion.Lerp(swingPosition.transform.localRotation, defaultSwingPosition.localRotation, 10 * Time.deltaTime);
        }


        // apply gravity
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

    }

    void PlayFootstepSound()
    {
        if(footstepReady)
        {
            soundManager.PlayPlayerFootstepSound(this.audioSource);
            footStepCooldown = footstepFrequencyModifier / currentSpeed;
            footstepReady = false;
            Invoke("ResetStep", footStepCooldown);

        }
    }

    void ResetStep()
    {
        footstepReady = true;
    }

}
