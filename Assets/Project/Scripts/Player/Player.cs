﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

public class Player : MonoBehaviour
{

    [Header("-Stats-")]
    public int health;
    public int maxHealth;

    [Header("-Inventory-")]
    public string equippedWeaponType;
    public int pistolAmmo;
    public int rifleAmmo;

    [Header("-Conditions-")]
    public bool isMoving;
    public bool isJumping;
    public bool isSprinting;
    public bool isCrouching;
    public bool isZooming;
    public static bool playerIsDead;


    [Header("-Other-")]
    public AudioSource utilityAudioSource;


    AudioSource audioSource;
    

    GameManager gameManager;
    SoundManager soundManager;

    bool fKeyPressed;


    // Start is called before the first frame update
    void Start()
    {
        ClearConsole();
        audioSource = this.gameObject.GetComponent<AudioSource>();
        gameManager = GameManager.Instance;
        soundManager = SoundManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ClearConsole();
        }

        //fKeyPressed = Input.GetKey(KeyCode.F);

    }

    private void FixedUpdate()
    {
    }

    public void ClearConsole()
    {
        //var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
        //var type = assembly.GetType("UnityEditor.LogEntries");
        //var method = type.GetMethod("Clear");
        //method.Invoke(new object(), null);
    }


    public void TakeDamage(int damage)
    {
        health -= damage;
        soundManager.PlayZombieBiteSound(this.audioSource);

        if ( health <= 0)
        {
            playerIsDead = true;
            gameManager.PlayerDied();
        }
    }



    private void OnTriggerStay(Collider other)
    {
        PickableItem tempItem = other.gameObject.GetComponent<PickableItem>();

        //Debug.Log(other.gameObject.name);
        if (tempItem)
        {

            //if pick up panel is hidden , show it
            if ( ! gameManager.pickUpPanel.active)
            {
                gameManager.ShowPickUpPanel();
            }


            // press F
            if (Input.GetKey(KeyCode.F))
            {

                // adding health
                if (this.maxHealth == 200)
                {
                    // max health reached maximum

                    if (tempItem.health + this.health > 200)
                    {
                        // picking up medkit would be overheal
                        this.health = this.maxHealth;
                    }
                    else
                    {
                        this.health = this.health + tempItem.health;
                    }

                }
                else
                {
                    // theres some hp max that can be added
                    if (tempItem.health + this.health > 200)
                    {
                        // picking up medkit would be overheal
                        this.maxHealth = 200;
                        this.health = this.maxHealth;
                    }
                    else
                    {
                        // max max hp not reached yet
                        this.health = this.health + tempItem.health;
                        if (this.health > this.maxHealth)
                        {
                            this.maxHealth = this.health;
                        }
                    }
                }

                // adding ammo
                pistolAmmo += tempItem.pistolAmmo;
                rifleAmmo += tempItem.rifleAmmo;

                // playing sound
                soundManager.PlayPickUpSound(utilityAudioSource);

                // disabling item
                tempItem.gameObject.SetActive(false);

                // hiding pick up panel
                gameManager.HidePickUpPanel();
            }
        }
    }



    private void OnTriggerExit(Collider other)
    {
        //hide pick up panel

        PickableItem tempItem = other.gameObject.GetComponent<PickableItem>();

        if (tempItem)
        {
            gameManager.HidePickUpPanel();
        }
    }


}
