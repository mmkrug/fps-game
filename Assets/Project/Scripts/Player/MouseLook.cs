﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public Player player;

    public float horizontalSensitivity = 300f;
    public float verticalSensitivity = 250f;

    private float mouseX;
    private float mouseY;

    public float maxClamp = 90f;
    public float minClamp = -90f;

    public Transform playerBody;
    public Camera playerCamera;

    float xRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;


        // reading changed sensitivities sensitivities
        if (!PlayerPrefs.HasKey("mouseVertical"))
        {
            PlayerPrefs.SetFloat("mouseVertical", 0.3f);
            verticalSensitivity = PlayerPrefs.GetFloat("mouseVertical") * 1000;
        }
        else
        {   
            verticalSensitivity = PlayerPrefs.GetFloat("mouseVertical") * 1000;
        }

        if (!PlayerPrefs.HasKey("mouseHorizontal"))
        {
            PlayerPrefs.SetFloat("mouseHorizontal", 0.25f);
            horizontalSensitivity = PlayerPrefs.GetFloat("mouseHorizontal") * 1000;
        }
        else
        {
            horizontalSensitivity = PlayerPrefs.GetFloat("mouseHorizontal") * 1000;
        }

    }

    // Update is called once per frame
    void Update()
    {

        // is holding righ mouse button?
        if (Input.GetMouseButton(1) && ! player.isSprinting)
        {
            mouseX = Input.GetAxis("Mouse X") * horizontalSensitivity * 0.4f * Time.deltaTime;
            mouseY = Input.GetAxis("Mouse Y") * verticalSensitivity * 0.4f * Time.deltaTime;
            player.isZooming = true;
        }
        else
        {
            mouseX = Input.GetAxis("Mouse X") * horizontalSensitivity * Time.deltaTime;
            mouseY = Input.GetAxis("Mouse Y") * verticalSensitivity * Time.deltaTime;
            player.isZooming = false;
        }

        

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, minClamp, maxClamp);


        playerCamera.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);

    }

    private void OnEnable()
    {
        if (Cursor.lockState == CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    private void FixedUpdate()
    {

    }
}
