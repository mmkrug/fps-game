﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    private static SoundManager instance;
    public static SoundManager Instance { get { return instance; } }


    public AudioSource audioSource;
    public AudioSource ricocheteAudioSource;
    public AudioSource bulletCaseAudioSource;

    [Header("-Pitch-")]
    public float minPitch = 0.8f;
    public float maxPitch = 1.2f;

    [Header("-Pistol Sounds-")]
    public AudioClip pistolShotSound;
    public AudioClip pistolReloadSound;
    public AudioClip pistolEquipSound;

    [Header("-Rifle Sounds-")]
    public AudioClip rifleShotSound;
    public AudioClip rifleReloadSound;
    public AudioClip rifleEquipSound;

    [Header("-Ricochet sounds-")]
    public AudioClip hardRicochetSound;
    public AudioClip softRicochetSound;

    [Header("-Bullet Shell sounds-")]
    public AudioClip shellSound1;
    public AudioClip shellSound2;

    [Header("-Zombie Bullet Impact Sounds-")]
    public AudioClip impactSound1;
    public AudioClip impactSound2;

    [Header("-Zombie Hurt Sounds-")]
    public AudioClip zombieHurtSound1;
    public AudioClip zombieHurtSound2;

    [Header("-Zombie Idle Sounds-")]
    public AudioClip zombieIdleSound1;
    public AudioClip zombieIdleSound2;

    [Header("-Zombie Attack Sounds-")]
    public AudioClip zombieAttackSound1;
    public AudioClip zombieAttackSound2;

    [Header("-Zombie Start Chase Sounds-")]
    public AudioClip zombieStartChaseSound1;
    public AudioClip zombieStartChaseSound2;

    [Header("-Player Footstep Sounds-")]
    public AudioClip playerFootstepSound1;
    public AudioClip playerFootstepSound2;

    [Header("-Zombie Footstep Sounds-")]
    public AudioClip zombieFootstepSoundWalk;
    public AudioClip zombieFootstepSoundRun;

    [Header("-Zombie Bite Sounds-")]
    public AudioClip zombieBiteSound1;
    public AudioClip zombieBiteSound2;

    [Header("-Pick Up Sounds-")]
    public AudioClip pickUpSound1;

    [Header("-Horde Music-")]
    public AudioClip hordeMusic1;
    public AudioClip hordeMusic2;
    public AudioClip hordeMusic3;
    public AudioClip hordeMusic4;





    //private AudioSource pistolAudioSource;
    //private AudioClip pistolShotSound;


    private float randomPitch;


    void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

        //pistolAudioSource = GetComponent<AudioSource>();
        //pistolShotSound = Folder.Load<AudioClip>("PistolShotSound");


    }


    public void PlayShotSound(string weaponType)
    {
        randomPitch = Random.Range(minPitch, maxPitch);
        audioSource.pitch = randomPitch;

        switch (weaponType)
        {
            case "pistol":
                audioSource.PlayOneShot(pistolShotSound);
                break;

            case "rifle":
                audioSource.PlayOneShot(rifleShotSound);
                break;

            default: break;
        }

    }


    public void PlayReloadSound(string weaponType)
    {
        audioSource.pitch = 1;

        switch (weaponType)
        {
            case "pistol":
                audioSource.PlayOneShot(pistolReloadSound);
                break;

            case "rifle":
                audioSource.PlayOneShot(rifleReloadSound);
                break;

            default: break;
        }
    }


    public void PlayEquipSound(string weaponType)
    {
        randomPitch = Random.Range(minPitch, maxPitch);

        switch (weaponType)
        {
            case "pistol":
                audioSource.pitch = randomPitch;
                audioSource.PlayOneShot(pistolEquipSound);
                break;

            case "rifle":
                audioSource.pitch = randomPitch;
                audioSource.PlayOneShot(rifleEquipSound);
                break;

            default: break;
        }
    }

    public void PlayRicocheteSound(string ricocheteType, Vector3 position)
    {
        randomPitch = Random.Range(minPitch, maxPitch);
        ricocheteAudioSource.pitch = randomPitch;

        // only 30% to trigger the sound effect
        if (Random.value > 0.3) return;

        switch (ricocheteType)
        {
            case "soft":
                this.ricocheteAudioSource.transform.position = position;
                ricocheteAudioSource.PlayOneShot(softRicochetSound);
                break;

            case "hard":
                this.ricocheteAudioSource.transform.position = position;
                ricocheteAudioSource.PlayOneShot(hardRicochetSound);
                break;
        }
    }




    public void PlayBulletCaseSound(int num, Vector3 position)
    {
        randomPitch = Random.Range(minPitch, maxPitch);
        bulletCaseAudioSource.pitch = randomPitch;

        bulletCaseAudioSource.transform.position = position;


        switch (num)
        {
            case 1:
                bulletCaseAudioSource.PlayOneShot(shellSound1);
                break;

            case 2:
                bulletCaseAudioSource.PlayOneShot(shellSound2);
                break;

        }

    }


    public void PlayPlayerFootstepSound(AudioSource audioSource)
    {
        randomPitch = Random.Range(minPitch, maxPitch);
        audioSource.pitch = randomPitch;

        if (Random.value > 0.5)
        {
            audioSource.PlayOneShot(playerFootstepSound1);
        }
        else
        {
            audioSource.PlayOneShot(playerFootstepSound2);
        }
    }


    public void PlayImpactSound(AudioSource audioSource)
    {
        randomPitch = Random.Range(minPitch, maxPitch);
        audioSource.pitch = randomPitch;

        if (Random.value > 0.5)
        {
            audioSource.PlayOneShot(impactSound1);
        }
        else
        {
            audioSource.PlayOneShot(impactSound2);
        }
    }


    public void PlayZombieHurtSound(AudioSource audioSource)
    {
        //randomPitch = Random.Range(minPitch, maxPitch);
        //audioSource.pitch = randomPitch;

        if (Random.value > 0.5)
        {
            audioSource.PlayOneShot(zombieHurtSound1);
        }
        else
        {
            audioSource.PlayOneShot(zombieHurtSound2);
        }
    }

    public void PlayZombieIdleSound(AudioSource audioSource)
    {
        //randomPitch = Random.Range(minPitch, maxPitch);
        //audioSource.pitch = randomPitch;

        if (Random.value > 0.5)
        {
            audioSource.PlayOneShot(zombieIdleSound1);
        }
        else
        {
            audioSource.PlayOneShot(zombieIdleSound2);
        }
    }

    public void PlayZombieStartChaseSound(AudioSource audioSource)
    {
        //randomPitch = Random.Range(minPitch, maxPitch);
        //audioSource.pitch = randomPitch;

        if (Random.value > 0.5)
        {
            audioSource.PlayOneShot(zombieStartChaseSound1);
        } 
        else
        {
            audioSource.PlayOneShot(zombieStartChaseSound2);
        }
    }




    public void PlayZombieAttackSound(AudioSource audioSource)
    {
        //randomPitch = Random.Range(minPitch, maxPitch);
        //audioSource.pitch = randomPitch;

        if (Random.value > 0.5)
        {
            audioSource.PlayOneShot(zombieAttackSound1);
        }
        else
        {
            audioSource.PlayOneShot(zombieAttackSound2);
        }
    }

    public void PlayZombieBiteSound(AudioSource audioSource)
    {
        //randomPitch = Random.Range(minPitch, maxPitch);
        //audioSource.pitch = randomPitch;

        if (Random.value > 0.5)
        {
            audioSource.PlayOneShot(zombieBiteSound1);
        }
        else
        {
            audioSource.PlayOneShot(zombieBiteSound2);
        }
    }


    public void PlayPickUpSound(AudioSource audioSource)
    {
        audioSource.pitch = 1;
        audioSource.PlayOneShot(pickUpSound1);
    }


}
