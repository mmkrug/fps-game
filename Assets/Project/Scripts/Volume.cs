﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Volume : MonoBehaviour
{

    Slider volumeSlider;

    public void Start()
    {
        volumeSlider = GetComponent<Slider>();

        if( ! PlayerPrefs.HasKey("volume"))
        {
            PlayerPrefs.SetFloat("volume", 0.4f);
            Load();
        } else
        {
            Load();
        }
    }



    public void ChangeVolume()
    {
        AudioListener.volume = volumeSlider.value;
        Save();
    }


    void Load()
    {
        volumeSlider.value = PlayerPrefs.GetFloat("volume");
    }

    void Save()
    {
        PlayerPrefs.SetFloat("volume", volumeSlider.value);
    }

}
