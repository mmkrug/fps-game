﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper : MonoBehaviour
{

    private static Helper instance;
    public static Helper Instance { get { return instance; } }

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }


    public void sortHits(RaycastHit[] hits)
    {
        if (hits.Length < 2) return;

        RaycastHit temp;

        for (int write = 0; write < hits.Length; write++)
        {
            for (int sort = 0; sort < hits.Length - 1; sort++)
            {
                if (hits[sort].distance > hits[sort + 1].distance)
                {
                    temp = hits[sort + 1];
                    hits[sort + 1] = hits[sort];
                    hits[sort] = temp;
                }
            }
        }

         

    }


}
