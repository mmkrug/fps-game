﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolingManager : MonoBehaviour
{
    private static ObjectPoolingManager instance;
    public static ObjectPoolingManager Instance { get { return instance; } }

    [System.Serializable]
    public class Pool
    {
        public string name;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;


    IEnumerator DisableAfterDelayRoutine(GameObject gameObject, float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }


    // Start is called before the first frame update
    void Awake()
    {
        instance = this;




    }

    private void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.name, objectPool);
        }

        //initializeQueue(fleshBulletHoles, fleshBulletHole);

    }


    private void Update()
    {

    }

    public GameObject spawnFromPool (string name)
    {

        GameObject objectToSpawn = null;

        if ( ! poolDictionary.ContainsKey(name))
        {
            Debug.LogWarning("There's no pool with name: " + name);
            return null;
        }


        // if the first item in the queue is active
        if (poolDictionary[name].Peek().activeSelf)
        {
            GameObject tempPrefab = null;
            // finding prefab for the object to spawn
            foreach (Pool pool in pools)
            {
                if (pool.name.Equals(name))
                {
                    tempPrefab = pool.prefab;
                }
            }

            if (tempPrefab == null)
            {
                Debug.LogWarning("Wrong pool prefab");
                return null;
            }

            objectToSpawn = Instantiate(tempPrefab);

        }
        else
        {
            objectToSpawn = poolDictionary[name].Dequeue();
        }

        objectToSpawn.SetActive(true);
        poolDictionary[name].Enqueue(objectToSpawn);
        return objectToSpawn;
    }




}
