﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSoundManager : MonoBehaviour
{
    public AudioClip backgroundMusic;
    public AudioClip buttonClick;

    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();


        // changing volume to lower value
        if ( ! PlayerPrefs.HasKey("volume"))
        {
            PlayerPrefs.SetFloat("volume", 0.4f);
            AudioListener.volume = PlayerPrefs.GetFloat("volume");
        }
        else
        {
            AudioListener.volume = PlayerPrefs.GetFloat("volume");
        }



        audioSource.clip = backgroundMusic;
        audioSource.loop = true;
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            PlayButtonClick();
    }


    public void PlayButtonClick()
    {
        audioSource.PlayOneShot(buttonClick);
    }

}
