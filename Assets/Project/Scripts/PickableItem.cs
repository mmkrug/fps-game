﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItem : MonoBehaviour
{
    public int health;
    public int pistolAmmo;
    public int rifleAmmo;

    public Vector2 healthRand;
    public Vector2 pistolAmmoRand;
    public Vector2 rifleAmmoRand;

    SoundManager soundManager;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        soundManager = SoundManager.Instance;
        audioSource = GetComponent<AudioSource>();

        health = (int)Random.Range(healthRand.x, healthRand.y);
        pistolAmmo = (int)Random.Range(pistolAmmoRand.x, pistolAmmoRand.y);
        rifleAmmo = (int)Random.Range(rifleAmmoRand.x, rifleAmmoRand.y);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void DeactivateItem()
    {
        this.gameObject.SetActive(false);
    }

}
