﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieAi : MonoBehaviour
{
    public GameObject player;
    public LayerMask whatIsGround, whatisPlayer;

    public Vector3 walkPoint;
    public float walkPointRange;

    public float walkSpeed;
    public float runSpeed;

    public float timeBetweenAttacks;
    bool alreadyAttacked;

    public float sightRange, attackRange;
    public bool walkPointSet;
    public bool playerInSightRange;
    public bool playerInAttackRange;

    public bool isPatrolling;
    public bool isChasing;
    public bool isAttacking;



    public float screamCooldown;

    NavMeshAgent agent;
    Animator animator;
    AudioSource audioSource;
    SoundManager soundManager;


    bool forceChase = false;
    float defaultAgentSpeed;
    public bool isScreamReady = true;



    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        audioSource = GetComponent<AudioSource>();
        defaultAgentSpeed = walkSpeed;
    }


    // Start is called before the first frame update
    void Start()
    {
        isPatrolling = false;
        isChasing = false;
        isAttacking = false;



        animator = GetComponent<Animator>();
        soundManager = SoundManager.Instance;
        
    }

    // Update is called once per frame
    void Update()
    {

        // check for sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatisPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatisPlayer);

        if ( ! forceChase)
        {
            // normal scenario
            if (!playerInSightRange && !playerInAttackRange) Patrolling();
            if (playerInSightRange && !playerInAttackRange) ChasePlayer();
            if (playerInAttackRange) AttackPlayer();
        }else
        {
            // zombie was attacked
            if (playerInAttackRange)
            {
                AttackPlayer(); 
            }   else
            {
                ChasePlayer();
            }

        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        // walkpoint reached
        if (distanceToWalkPoint.magnitude < 2f)
            walkPointSet = false;

        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);
    }



    void Patrolling()
    {
        if(isPatrolling == false)
        {
            // just switching to this state
            //audioSource.Stop();
            audioSource.clip = soundManager.zombieFootstepSoundWalk;
            audioSource.Play();
            
        } else
        {
            // patrolling state persists
            if( ! audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }


        isPatrolling = true;
        isChasing = false;
        isAttacking = false;

        animator.SetBool("isPatrolling", isPatrolling);
        animator.SetBool("isChasing", isChasing);
        animator.SetBool("isAttacking", isAttacking);

        agent.speed = Mathf.Lerp(agent.speed, defaultAgentSpeed, Time.deltaTime);


        if ( ! walkPointSet)
        {
            float randomX = Random.Range(-walkPointRange, walkPointRange);
            float randomZ = Random.Range(-walkPointRange, walkPointRange);

            walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

            //Debug.Log(walkPoint);
            Debug.DrawRay(walkPoint, transform.up * 200, Color.yellow, 20);


            if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
                walkPointSet = true;

            if (walkPointSet)
                agent.SetDestination(walkPoint);
        }



    }

    void ChasePlayer()
    {
        if (isChasing == false)
        {
            // just switching to this state
            //audioSource.Stop();
            audioSource.clip = soundManager.zombieFootstepSoundRun;
            audioSource.Play();

            this.PlayScreamSound("startChase");
            
        } else
        {
            // chasing state persists
            if ( ! audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }


        isPatrolling = false;
        isChasing = true;
        isAttacking = false;

        animator.SetBool("isPatrolling", isPatrolling);
        animator.SetBool("isChasing", isChasing);
        animator.SetBool("isAttacking", isAttacking);


        agent.speed = Mathf.Lerp(agent.speed, runSpeed, Time.deltaTime);


        agent.SetDestination(player.transform.position);

    }


    public void ForceChasePlayer()
    {
        this.forceChase = true;
    }


    void AttackPlayer()
    {
        if (isAttacking == false)
        {
            // just switching to this state
            if (isScreamReady)
            {
                isScreamReady = false;
                audioSource.Stop();
                soundManager.PlayZombieAttackSound(this.audioSource);
                Invoke(nameof(ResetScream), screamCooldown);
            }
        }
        else
        {
            if (isScreamReady)
            {
                isScreamReady = false;
                soundManager.PlayZombieAttackSound(this.audioSource);
                Invoke(nameof(ResetScream), screamCooldown);
            }
        }

        isPatrolling = false;
        isChasing = false;
        isAttacking = true;

        animator.SetBool("isPatrolling", isPatrolling);
        animator.SetBool("isChasing", isChasing);
        animator.SetBool("isAttacking", isAttacking);

        // make sure enemy doesn't move
        agent.SetDestination(transform.position);
        transform.LookAt(player.transform.position);

        // attack the player
        if( ! alreadyAttacked)
        {
            // this is the first attack
            alreadyAttacked = true;
            player.GetComponent<Player>().TakeDamage(10);

            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    void ResetAttack()
    {
        alreadyAttacked = false;
    }



    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }



    void PlayScreamSound(string type)
    {
        if ( !isScreamReady)
        {
            return;
        }

        isScreamReady = false;

        switch (type)
        {
            // idle scream is handled in the Zombie script
            //case "idle":
            //    soundManager.PlayZombieIdleSound(this.audioSource);
            //    break;

            case "startChase":
                soundManager.PlayZombieStartChaseSound(this.audioSource);
                break;

            case "attack":
                soundManager.PlayZombieAttackSound(this.audioSource);
                break;

            default:
                break;
        }

        Invoke(nameof(ResetScream), screamCooldown);

    }

    void ResetScream()
    {
        isScreamReady = true;
    }

    public void SetPosition(Vector3 position)
    {
        agent.Warp(position);
        //Debug.Log("changed position: " + transform.position);
    }

}
