﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    public bool isDead = false;

    private int maxHealth = 100;
    public int currentHealth;
    

    public GameObject zombieObject;
    public GameObject humanPelvis;
    public GameObject humanPelvisHead;
    public Animator animator;

    ZombieAi zombieAi;
    NavMeshAgent agent;
    AudioSource audioSource;
    SoundManager soundManager;
    GameManager gameManager;
    ObjectPoolingManager objectPoolingManager;

    // Start is called before the first frame update
    void Awake()
    {
        this.currentHealth = this.maxHealth;
        zombieAi = transform.GetComponent<ZombieAi>();
        agent = transform.GetComponent<NavMeshAgent>();
        audioSource = GetComponent<AudioSource>();



        //// activating zombie
        //SetAnimator(true);
        //SetZombieCollider(true);
        //SetZombieKinematic(false);

        //// disabling ragdoll, leaving colliders
        //SetPelvisKinematic(true);
        //SetPelvisTrigger(true);

        SetTags();
    }

    private void Start()
    {
        soundManager = SoundManager.Instance;
        gameManager = GameManager.Instance;
        objectPoolingManager = ObjectPoolingManager.Instance;
        Invoke("PlayIdleSound", Random.Range(5, 30));

    }

    private void OnEnable()
    {
        this.agent.enabled = true;
        zombieAi.enabled = true;
        isDead = false;

        // activating zombie
        SetAnimator(true);
        SetZombieCollider(true);
        SetZombieKinematic(false);

        // disabling ragdoll, leaving colliders
        SetPelvisKinematic(true);
        SetPelvisTrigger(true);

        GetComponent<DecalDeactivator>().enabled = false;
    }



    public void TakeDamage(int damage)
    {
        this.currentHealth -= damage;
        zombieAi.ForceChasePlayer();

        soundManager.PlayImpactSound(this.audioSource);

        if (Random.value < 0.3)
        {
            soundManager.PlayZombieHurtSound(this.audioSource);
        }

        if (currentHealth <= 0)
        {
            die();
        }
    }

    public void die()
    {
        isDead = true;
        zombieAi.enabled = false;
        agent.enabled = false;

        SetZombieCollider(false);
        SetZombieKinematic(true);
        SetAnimator(false);

        SetPelvisKinematic(false);
        SetPelvisTrigger(false);

        audioSource.Stop();

        gameManager.zombiesAlive--;

        if (Random.value < 0.3)
        {
            GameObject supplies = objectPoolingManager.spawnFromPool("SuppliesPool");
            supplies.transform.position = this.transform.position;
            supplies.SetActive(true);
        }

        GetComponent<DecalDeactivator>().enabled = true;
    }

    private void SetZombieCollider(bool isActive)
    {
        //this.zombieObject.GetComponent<BoxCollider>().enabled = isActive;
        this.zombieObject.GetComponent<Collider>().enabled = isActive;
    }

    private void SetZombieKinematic(bool isActive)
    {
        this.zombieObject.GetComponent<Rigidbody>().isKinematic = isActive;
    }

    private void SetPelvisKinematic(bool isActive)
    {
        Rigidbody[] rigidbodys = this.humanPelvis.GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody r in rigidbodys)
        {
            r.isKinematic = isActive;
        }
    }

    private void SetPelvisTrigger(bool isActive)
    {
        Collider[] colliders = this.humanPelvis.GetComponentsInChildren<Collider>();

        foreach(Collider c in colliders)
        {
            //c.enabled = isActive;
            c.isTrigger = isActive;
        }


    }


    private void SetAnimator(bool isActive)
    {
        this.animator.enabled = isActive;
    }


    private void SetTags()
    {

        Transform[] transforms = this.humanPelvis.GetComponentsInChildren<Transform>();

        foreach(Transform t in transforms)
        {
            t.tag = "Zombie_Body";
        }

        this.humanPelvisHead.tag = "Zombie_Head";
    }


    void PlayIdleSound()
    {
        if( !isDead)
        {
            soundManager.PlayZombieIdleSound(this.audioSource);
            Invoke("PlayIdleSound", Random.Range(5, 30));
        }
    }

}
