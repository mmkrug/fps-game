﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
//using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{

    public GameObject DeathScreenMenu;

    public TextMeshProUGUI waveText;
    public TextMeshProUGUI killsText;
    public TextMeshProUGUI scoreText;



    GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        

    }


    public void CalculateScores()
    {
        waveText.text = "" + gameManager.wave;
        killsText.text = "" + gameManager.killed;
        scoreText.text = "" + gameManager.score;
    }







    //public void Resume()
    //{
    //    pauseMenu.SetActive(false);
    //    Time.timeScale = 1f;
    //    gameManager.ActivateGunScript();
    //    gameIsPaused = false;

    //    if (Cursor.lockState == CursorLockMode.None)
    //    {
    //        Cursor.lockState = CursorLockMode.Locked;
    //        Cursor.visible = false;
    //    }



    //    // updating mouse sensitivities
    //    if (!PlayerPrefs.HasKey("mouseVertical"))
    //    {
    //        PlayerPrefs.SetFloat("mouseVertical", 0.3f);
    //        gameManager.ChangeVerticalSensitivity(PlayerPrefs.GetFloat("mouseVertical") * 1000);
    //    }
    //    else
    //    {
    //        gameManager.ChangeVerticalSensitivity(PlayerPrefs.GetFloat("mouseVertical") * 1000);
    //    }

    //    if (!PlayerPrefs.HasKey("mouseHorizontal"))
    //    {
    //        PlayerPrefs.SetFloat("mouseHorizontal", 0.25f);
    //        gameManager.ChangeHorizontalSensitivity(PlayerPrefs.GetFloat("mouseVertical") * 1000);
    //    }
    //    else
    //    {
    //        gameManager.ChangeHorizontalSensitivity(PlayerPrefs.GetFloat("mouseVertical") * 1000);
    //    }
    //}

    //void Pause()
    //{
    //    pauseMenu.SetActive(true);
    //    Time.timeScale = 0f;
    //    gameIsPaused = true;
    //    gameManager.DeactivateGunScript();

    //    if (Cursor.lockState == CursorLockMode.Locked)
    //    {
    //        Cursor.lockState = CursorLockMode.None;
    //        Cursor.visible = true;
    //    }
    //}



    ////public void PlayGame()
    ////{
    ////    SceneManager.LoadScene(1);
    ////}

    public void QuitGame()
    {
        Application.Quit();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        //Resume();
    }

    public void ChangeSceneToMainMenu()
    {
        SceneManager.LoadScene(0);
    }


}
