﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponCrosshair : MonoBehaviour
{

    public Image top;
    public Image bottom;
    public Image right;
    public Image left;
    public Image dot;

    public float lineLength = 14;
    public float lineWidth = 2;

    public float dotSize = 2;

    public float spacing = 30;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnEnable()
    {
        top.rectTransform.sizeDelta = new Vector2(lineWidth, lineLength);
        top.transform.localPosition = new Vector3(0f, spacing, 0f);

        bottom.rectTransform.sizeDelta = new Vector2(lineWidth, lineLength);
        bottom.transform.localPosition = new Vector3(0f, -spacing, 0f);

        right.rectTransform.sizeDelta = new Vector2(lineLength, lineWidth);
        right.transform.localPosition = new Vector3(spacing, 0f, 0f);

        left.rectTransform.sizeDelta = new Vector2(lineLength, lineWidth);
        left.transform.localPosition = new Vector3(-spacing, 0f, 0f);

        dot.rectTransform.sizeDelta = new Vector2(dotSize, dotSize);
    }
}
