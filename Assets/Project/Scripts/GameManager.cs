﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }


    [Header("-Game-")]
    public Player player;
    public GameObject weaponHolder;
    public LayerMask whatIsGround;
    public GameObject enemySpawnPointsGroup;
    public float gameVolume = 0.4f;

    [Header("-UI-")]
    public TextMeshProUGUI currentWeaponAmmo;
    public TextMeshProUGUI inventoryAmmoText;
    public TextMeshProUGUI currentScore;
    public RectTransform crosshair;
    public float crosshairSpreadModifier = 100;
    public RectTransform hitMarker;
    public Slider healthbar;
    public TextMeshProUGUI healthText;
    public GameObject pickUpPanel;

    Gun activeGun;
    CanvasGroup crosshairCanvas;
    CanvasGroup hitMarkerCanvas;
    List<Image> hitMarkerImages;
    GameObject tempZombie;
    List<Transform> enemySpawnPoints;
    MouseLook playerMouse;

    ObjectPoolingManager objectPoolingManager;

    public int score = 0;
    public int killed = 0;
    public int killedThisWave = 0;
    public int zombiesAlive = 0;
    public int wave = 0;
    public int waveForceChase = 4;

    bool isGamePaused = false;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        crosshairCanvas = crosshair.GetComponent<CanvasGroup>();
        hitMarkerCanvas = hitMarker.GetComponent<CanvasGroup>();
        objectPoolingManager = ObjectPoolingManager.Instance;
        playerMouse = player.GetComponent<MouseLook>();

        hitMarkerImages = new List<Image>();
        foreach (Image image in this.hitMarker.GetComponentsInChildren<Image>())
        {
            hitMarkerImages.Add(image);
        }


        enemySpawnPoints = new List<Transform>();
        foreach (Transform child in enemySpawnPointsGroup.transform)
        {
            enemySpawnPoints.Add(child.transform);
            //Debug.Log("Tr Pos: " + child.transform.position);
        }

        //SpawnZombies(10);

    }

    private void OnEnable()
    {
        Time.timeScale = 1f;

    }

    // Update is called once per frame
    void Update()
    {
        // updating objects
        activeGun = getActiveGun();

        if (killedThisWave >= wave
            && wave > 0)
        {
            wave++;
            SpawnZombies(wave*2);
        }





        // crosshair management
        // if zooming, fade out
        if (player.isZooming)
        {
            if (crosshairCanvas.alpha > 0.0001f)
                crosshairCanvas.alpha = Mathf.Lerp(crosshairCanvas.alpha, 0, 10 * Time.deltaTime);
        } else
        {
            if (crosshairCanvas.alpha < 0.95f)
                crosshairCanvas.alpha = Mathf.Lerp(crosshairCanvas.alpha, 1, 10 * Time.deltaTime);
        }
        
        // crosshair spacing
        Vector2 tempCrosshairSize = new Vector2(activeGun.spread * crosshairSpreadModifier, activeGun.spread * crosshairSpreadModifier);
        crosshair.sizeDelta = Vector2.Lerp(crosshair.sizeDelta, tempCrosshairSize, 10 * Time.deltaTime);


        // hit marker
        if (hitMarkerCanvas.alpha > 0.0001f)
            hitMarkerCanvas.alpha = Mathf.Lerp(hitMarkerCanvas.alpha, 0, 5 *  Time.deltaTime);


        // update score
        currentScore.text = "Level: " + wave
                + "\nZombies Alive: " + zombiesAlive
                + "\n\nScore: " + score
                + "\nKills: " + killed;

        if (wave >= waveForceChase)
        {
            currentScore.color = Color.red;
        }

        // update ivnentory ammo count
        switch (player.equippedWeaponType)
        {
            case "pistol":
                currentWeaponAmmo.text = getActiveWeaponAmmo().ToString();
                inventoryAmmoText.text = " / " + player.pistolAmmo.ToString();
                break;

            case "rifle":
                currentWeaponAmmo.text = getActiveWeaponAmmo().ToString();
                inventoryAmmoText.text = " / " + player.rifleAmmo.ToString();
                break;

            default: break;
        }


        // update healthbar
        healthbar.value = player.health;
        healthbar.maxValue = player.maxHealth;

        healthText.text = player.health + " / " + player.maxHealth;

        
    }


    public void triggerHitMarker(bool isKilled, string bodyPart)
    {
        Color color = Color.white;

        switch (bodyPart)
        {
            case "body":
                color = Color.white;
                break;
            case "head":
                color = Color.red;
                break;
        }

        foreach (Image image in hitMarkerImages)
        {
            image.color = color;
        }

        if (isKilled)
        {
            hitMarkerCanvas.alpha = 1;
            hitMarker.localScale = new Vector3(1.2f, 1.2f, 1);
        } else
        {
            hitMarker.localScale = new Vector3(1, 1, 1);
            hitMarkerCanvas.alpha = 0.4f;
        }
    }


    int getActiveWeaponAmmo()
    {
        foreach (Transform weapon in weaponHolder.transform)
        {
            if (weapon.gameObject.activeSelf)
            {
                return weapon.gameObject.GetComponent<Gun>().bulletsLeft;
            }
        }

        return 0;
    }

    Gun getActiveGun()
    {
        foreach (Transform weapon in weaponHolder.transform)
        {
            if (weapon.gameObject.activeSelf)
            {
                return weapon.gameObject.GetComponent<Gun>();
            }
        }
        return null;
    }

    public void SpawnZombies(int ammount)
    {
        //return;
        //Debug.Log("spawn");

        // finding the closest spawn point
        float closestSpawnPointDistance = -1;
        
        foreach( Transform enemySpawnPoint in enemySpawnPoints)
        {
            //Debug.Log("Point Tr Pos:" + enemySpawnPoint.position);
            //Debug.Log("Distance: " + Vector3.Distance(player.transform.position, enemySpawnPoint.position));

            if (closestSpawnPointDistance < 0)
            {
                // first iteration
                closestSpawnPointDistance = Vector3.Distance(player.transform.position, enemySpawnPoint.position);
            }
            else
            {
                float tempDistance = Vector3.Distance(player.transform.position, enemySpawnPoint.position);

                if (tempDistance < closestSpawnPointDistance)
                {
                    closestSpawnPointDistance = tempDistance;
                }
            }
        }


        int spawnPointIterator = 0;
        for (int i=0; i < ammount; i++)
        {
            // spawn #wave zombies

            tempZombie = objectPoolingManager.spawnFromPool("ZombiePool");

            // getting the NOT closest spawn point
            // if iterated spawn point is closer than the closest+0.1 then spawn for the next spawnpoint
            


            if (Vector3.Distance(player.transform.position, enemySpawnPoints[spawnPointIterator % enemySpawnPoints.Count].position)
                < closestSpawnPointDistance + 0.1f
                ){
                spawnPointIterator++;
            }

            tempZombie.GetComponent<ZombieAi>().SetPosition(enemySpawnPoints[spawnPointIterator % enemySpawnPoints.Count].position);
            spawnPointIterator++;


            if ( wave >= waveForceChase)
            {
                tempZombie.GetComponent<ZombieAi>().ForceChasePlayer();
            }

            zombiesAlive++;
        }

        killedThisWave = 0;
    }



    public void scoreKilled(string bodyPart)
    {
        switch (bodyPart)
        {
            case "body":
                score += 1;
                killed += 1;
                killedThisWave += 1;
                break;
            case "head":
                score += 3;
                killed += 1;
                killedThisWave += 1;
                break;
        }
    }

    public void ShowPickUpPanel()
    {
        this.pickUpPanel.SetActive(true);
    }

    public void HidePickUpPanel()
    {
        this.pickUpPanel.SetActive(false);
    }

    public void ChangeVerticalSensitivity(float newValue)
    {
        //Debug.Log(newValue);
        playerMouse.verticalSensitivity = newValue;
        //audioListener.volume = newVolume;
    }

    public void ChangeHorizontalSensitivity(float newValue)
    {
        //Debug.Log(newValue);
        playerMouse.horizontalSensitivity = newValue;
        //audioListener.volume = newVolume;
    }

    public void DeactivateGunScript()
    {
        activeGun.enabled = false;
    }

    public void ActivateGunScript()
    {
        activeGun.enabled = true;
    }

    public void PlayerDied()
    {

        GetComponent<DeathScreen>().DeathScreenMenu.SetActive(true);
        GetComponent<DeathScreen>().CalculateScores();
        DeactivateGunScript();

        Time.timeScale = 0f;

        if (Cursor.lockState == CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

}
